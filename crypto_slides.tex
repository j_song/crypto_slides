% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This file is a template using the "beamer" package to create slides for a talk or presentation
% - Giving a talk on some subject.
% - The talk is between 15min and 45min long.
% - Style is ornate.

% MODIFIED by Jonathan Kew, 2008-07-06
% The header comments and encoding in this file were modified for inclusion with TeXworks.
% The content is otherwise unchanged from the original distributed with the beamer package.

\documentclass[12pt]{beamer}


% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 


\mode<presentation>
{
  \usetheme{Warsaw}
  % or ...

  \setbeamercovered{transparent}
  % or whatever (possibly just delete it)
}


\usepackage[english]{babel}
% or whatever

\usepackage[utf8]{inputenc}
% or whatever
\usepackage{graphicx}
\usepackage{times}
\usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.


\title[] % (optional, use only with long paper titles)
{Topics in Cryptography}

\subtitle
{} % (optional)

\author[] % (optional, use only with lots of authors)
{Jialin Song}
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Universities of Toronto] % (optional, but mostly needed)
{ %
  Department of Mathematics \\
  University of Toronto}
% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.
\titlegraphic{\includegraphics[scale = 0.4]{UofT_Logo}}
\date{}
\subject{Talks}
% This is only inserted into the PDF information catalog. Can be left
% out. 



% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}



% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

%\beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}


% Since this a solution template for a generic talk, very little can
% be said about how it should be structured. However, the talk length
% of between 15min and 45min and the theme suggest that you stick to
% the following rules:  

% - Exactly two or three sections (other than the summary).
% - At *most* three subsections per section.
% - Talk about 30s to 2min per frame. So there should be between about
%   15 and 30 frames, all told.

\section{Introduction}

\subsection{Terminology}

\begin{frame}{Terminology}
  % - A title should summarize the slide in an understandable fashion
  %   for anyone how does not follow everything on the slide itself.

  \begin{itemize}
   \item<1->
   $\mathcal{K}$: all possible keys \\
   \item<1->
   $\mathcal{M}$: all possible messages \\
   \item<1->
   $\mathcal{C}$: all possible ciphertexts
   \vspace{1cm}
   \item<2->
   Encryption: an injective function $e$: $\mathcal{K} \times \mathcal{M} \rightarrow \mathcal{C}$
   \item<2->
   Decryption: a surjective function $d$: $\mathcal{K} \times \mathcal{C} \rightarrow \mathcal{M}$ with additional requirements depending on different ciphers
   \item<2->
   Cipher: an algorithm for performing encryption or decryption
  \end{itemize}
\end{frame}

\subsection{Example}
\begin{frame}{Substitution Cipher}

  \begin{itemize}
   \item 
   Perform alphabet substitutions in order to obtain a ciphertext from a plaintext
   \item
   Substitutions are done according to predefined rules 
  \end{itemize}
\end{frame}

\begin{frame}{Caesar cipher}
\begin{center}
\includegraphics[scale=0.25]{Caesar_cipher} \\
\footnotesize http://en.wikipedia.org/wiki/File:Caesar3.png \\
\vspace{0.5cm}
Example: hello $\rightarrow$ ebiil \\
\end{center}

\end{frame}

\begin{frame}{Cryptanalysis}
\begin{itemize}
  \item
  Simple substitutions do not mask frequency of characters in languages
  \item
  Statistical analysis on frequency of single characters, digrams and trigrams reveals substitution rules
\end{itemize}
\end{frame}

\subsection{Security}
\begin{frame}{Security}
  How do we know whether a cipher is secure? \\
  What do we mean by a secure cipher? \\
  \begin{itemize}
    \item
    Ciphertext does not reveal information about the key \\
   \pause
   Bad, since $e(k, m) = m$ satisfies this requirement
    \pause
    \item
    Ciphertext does not reveal all plaintext \\
    \pause
    Bad, since $e(k, m1 || m2) = m1 || e(k, m2)$ satisfies this requirement
    \pause
    \item
    Shannon's idea of perfect secrecy (1949): ciphertext should reveal no information about plaintext
  \end{itemize}
\end{frame}

\begin{frame}{Mathematical Formulation}
  \begin{itemize}
    \item
    (Shannon 1949) \\
    Definition: A cipher (e, d) over $(\mathcal{K}, \mathcal{M}, \mathcal{C})$ has perfect secrecy if $\forall m_0, m_1 \in \mathcal{M} (|m_0| = |m_1|),  \forall c \in \mathcal{C}, P(e(k, m_0) = c) = P(e(k, m_1) = c)$ if $k$ is drawn uniformly from $\mathcal{K}$
   \pause
   \item
   Essentially, it says that the ciphertext does not reveal any information about the plaintext.
  \end{itemize}
\end{frame}

\section{Symmetric Encryption}

\subsection{Definition}

\begin{frame}{Definition}
Encryption and decryption use the same key, \\
i.e. $\forall k \in \mathcal{K}, \forall m \in \mathcal{M}, d(k, e(k, m)) = m$
\end{frame}

\subsection{One-time pad}

\begin{frame}{One-time pad}
  \begin{itemize}
    \item
    For a given plaintext $m$ encoded in binary, the key $k$ is a random binary string of the same length, and the ciphtertext $c = m \oplus k$
    \item
    The decryption process uses the same key $k$ to obtain the plaintext $m = c \oplus k$
  \end{itemize}
\end{frame}

\begin{frame}{Security}
  \begin{itemize}
    \item
    One-time pad has perfect secrecy as defined by Shannon \\
    Proof:  for $m_0, m_1 \in \mathcal{M}$, suppose $|m_0| = |m_1| = L$, and for $c \in \mathcal{C}$, we want to prove that given that the key $k$ is drawn from $\mathcal{K}$ uniformly, $P(e(k, m_0) = c) = P(e(k, m_1) = c$.\\
   For one-time pad, $P(e(k, m_0) = c) = P(k = c \oplus m_0) = \frac{1}{2^L}$ \\
   $P(e(k, m_1) = c) = P(k = c \oplus m_1) = \frac{1}{2^L}$ \\
  So one-time pad has perfect secrecy. $\Box$
  \end{itemize}
\end{frame}

\begin{frame}{Drawbacks}
  \begin{itemize}
    \item
    The key has to be as long as the plaintext
    \item
    Each key can only be used once
  \end{itemize}
\end{frame}

\begin{frame}{Attacks on Multiple-Time Pad}
  \begin{itemize}
    \item
    when we use the same key $k$ to encrypt tow different messages $m_0$ and $m_1$
    \item
    $c_0 = k \oplus m_0$ 
    \item
    $c_1 = k \oplus m_1$
    \item
    $c_0 \oplus c_1  = (k \oplus m_0) \oplus (k \oplus m_1) = m_0 \oplus m_1$
    \item 
    we can exploit this information
  \end{itemize}
\end{frame}

\begin{frame}{Attacks on Multiple-Time Pad}
  Graphical Illustration:\\
  \begin{columns}[T]
    \begin{column}{.1\textwidth}
      \centering
      Plaintext \\
     \vspace{0.2cm}
      \includegraphics[scale = 0.4]{sendcash1} \\
     \vspace{0.3cm}
      \includegraphics[scale = 0.4]{smiley}
    \end{column}
    
    \begin{column}{.02\textwidth}
         \vspace{1.3cm}
        $\oplus$ \\
         \vspace{1.5cm}
        $\oplus$
    \end{column}

    \begin{column}{.1\textwidth}
      Key \\
      \vspace{0.2cm}
     \includegraphics[scale=0.4]{key1} \\
     \vspace{0.3cm}
     \includegraphics[scale=0.4]{key1}
    \end{column}
    
    \begin{column}{.02\textwidth}
       \vspace{1.4cm}   
        \hspace*{.1cm} $=$   \\
       \vspace{1.6cm}
      \hspace*{.1cm} $=$
    \end{column}

    \begin{column}{.15\textwidth}
     Ciphertext \\
     \vspace{0.2cm}
     \includegraphics[scale=.4]{sendcashe} \\
     \vspace{0.3cm}
     \includegraphics[scale=0.4]{smileye}
    \end{column}
  \end{columns}
   
   \vspace{.3cm}
   \hfill \footnotesize source: http://www.cryptosmith.com/archives/70
\end{frame}

\begin{frame}{Attacks on Multiple-Time Pad}
  When we xor the two ciphertexts which are encrypted using the same key.
  \begin{columns}[T]
    \begin{column}{.1\textwidth}
      \includegraphics[scale=.4]{sendcashe}
    \end{column}
   
    \begin{column}{.02\textwidth}
       \vspace{.7cm}
       $\oplus$
    \end{column}
     
    \begin{column}{.1\textwidth}
        \includegraphics[scale=.4]{smileye}
    \end{column}

    \begin{column}{.02\textwidth}
      \vspace{.7cm}
   \hspace*{.1cm} $=$
    \end{column}
   
    \begin{column}{.1\textwidth}
    \includegraphics[scale=.4]{smicash1}
    \end{column}
  \end{columns}
\end{frame}
\end{document}


